const pool = require('../../utils/database')
const security = require('../../utils/security')
const queryBuilder = require('../../utils/buildQuery')
const tokenUtil = require('../../utils/token')
const securityUtil = require('../../utils/security')

module.exports = {
    UsersGetFunc: (callback) => {
        let returnValue = {
            status: null,
            data: []
        };

        const query = queryBuilder.getUsersQ
    
        pool.query(
            query,
            (error, results) => {
                // console.log(results.rows)
                if(error) {
                    returnValue.status = false;
                    returnValue.data = [];
                } else {
                    returnValue.status = true;
                    returnValue.data = results.rows;
                }
                // console.log(returnValue)
                callback(returnValue);
            }
        )
    },
    UserGetByUsernameFunc: async (username, callback) => {
        let returnValue = {
            status: null,
            desc: '',
            data: {}
        };
        const query = queryBuilder.getUsersByUsernameQ;
        // console.info(query);

        await pool.query(query, [username], (error, results) => {
            // console.log(results)
            if(error) {
                returnValue.status = false;
                returnValue.desc = error.toString();
            } else {
                returnValue.status = true;
                if(results.rowCount < 1) {
                    returnValue.desc = "No data found!"
                } else {
                    returnValue.data = {
                        userId: results.rows[0].user_id,
                        email: results.rows[0].email
                    }
                }
            }
            return callback(returnValue);
        });
    },
    UsersPostFunc: async (userData, callback) => {
        console.log(userData);
        let returnValue = {
            status: null,
            desc: '',
            data: {}
        };

        let data = {
            userId:'',
            firstName: '',
        }

        // console.log(userData);
        const query = queryBuilder.PostUsersQ;
        
        let values = [
            userData.email,
            userData.emailVerificationStatus,
            userData.password,
            userData.firstName,
            userData.lastName,
            userData.userId
        ]

        try {
            pool.query(query, values, (error, results) => {
                if(!error) {
                    // console.log(results);
                    // const token = security.generateToken(results.rows[0].id);
                    returnValue.status = true;
                    returnValue.desc = '';
                    returnValue.data = {
                        // token: token,
                        // userId:results.rows[0].user_id,
                    }
                } else {
                    // console.log(results);
                    returnValue.status = false;
                    returnValue.desc = "User already registered!";
                }
                callback(returnValue)
            });
        } catch(error) {
            returnValue.status = false;
            returnValue.desc = error
            callback(returnValue)
        }
    },
    login: async (data, callback) => {
        let email = data.email;
        let password = data.password;
        let returnValue = {
            status: false,
            desc: '',
            data: {}
        }

        let query = queryBuilder.getUsersByEmailAddressQ;

        await pool.query(query, [email], (error, results) => {
            
            if(error) {
                returnValue.desc = error.toString();
            } else {
                if(results.rowCount < 1) {
                    returnValue.desc = 'Email Address Not Found!'
                } else {
                    let encryptedPassword = results.rows[0].encrypted_password;
                    let userId = results.rows[0].user_id.toString();
                    let dataToken = securityUtil.generateToken(userId);

                    returnValue.status = security.comparePassword(password, encryptedPassword.toString());
                    console.log(dataToken);
                    if(returnValue.status && dataToken.status) {

                        if(dataToken.status) {
                            returnValue.status = true;
                            returnValue.desc = "Password true";
                            returnValue.data = {
                                userId: results.rows[0].user_id,
                                email: results.rows[0].email,
                                accessToken: `${dataToken.token}`,
                                accessTokenType: `Bearer`
                            }
                        } else {
                            returnValue.desc = dataToken.desc;
                            // returnValue.status = false;
                        }
                        
                    } else {
                        returnValue.desc = "Password false!";
                    }
                }
            }
            return callback(returnValue);
        })
    },
    generateBasicToken: () => {
        let token = tokenUtil.generateBasicToken();
        // console.log(token);

        return token;
    }
}