const express = require('express')
const router = express.Router()
const {todos, tokenSuccess} = require('../../../dummy/db')
// const { UsersMePostFunc } = require('../../resources/users/me')
const UsersFunc = require('../../resources/users/users')
const securityUtils = require('../../utils/security') 

router.post('/', async (req, res) => {
    let email = req.body.email;
    let password = req.body.password;
    let firstName = req.body.firstName;
    let lastName = req.body.lastName;
    let userId = req.body.userId;
    let emailVerificationStatus = false;


    let status, message, code, data;

    if(email === '' || password === '' || firstName === '' || lastName === '' || userId === '') {
        code = 400;
        message = 'Please fill all columns!';
        res.status(422).send({
            code: 422,
            status: 'error',
            message: message
        })
    } else {
        let encryptedPassword = securityUtils.hashPassword(password);
        // console.log(encryptedPassword);
        let userData = {
            email:email,
            password: encryptedPassword,
            lastName:lastName,
            firstName: firstName,
            userId: userId,
            emailVerificationStatus: emailVerificationStatus
        };

        // console.log(userData);

        await UsersFunc.UsersPostFunc(userData, (dataReturn) => {
            if(!dataReturn.status) {
                code = 400;
                status = 'error';
                message = dataReturn.desc;
            } else {
                code = 201;
                status = 'success';
                message = '';
            }
            res.status(code).send({
                code: code,
                status: status,
                message: message
            })
        });
    };
    
})

router.get('/', async (req, res) => {
    // console.log(tokenSuccess);

    let status, message, items;

    let usersGet = {};
    await UsersFunc.UsersGetFunc((data) => {
        // console.log(" TEST WOYYY ")
        console.log(data.status)

        if(data.status) {
            status = "success";
            message = "";
            items = data.data;
        } else {
            status = "error";
            message = "error to fetch data to database";
            data = null;
        }

        res.status(200).send({
            code: 200,
            status: status,
            message: message,
            data: items
        })
    });
})

router.post('/me', async (req, res) => {
    let email = req.body.email;
    let password = req.body.password;
    let dataReq = {
        email, password
    }
    let code, status, message;

    await UsersFunc.login(dataReq, data => {
        if(data.status) {
            code = 200;
            status = "success";
            message = "";
        } else {
            code = 400;
            status = "error";
            message = data.desc;
        }
        res.status(code).send({
            code: code,
            status: status,
            message: message,
            data: data.data
        })
    })
})

router.get('/me', async (req, res) => {

    const authToken = req.header("Authorization");
    // console.log(authToken);

    try {
        let token = await UsersFunc.generateBasicToken();
        res.status(201).send({
            code: 201,
            status: "success",
            message: "",
            data: {
                accessToken: token
            }
        })
    } catch(error) {
        res.status(400).send({
            code: 400,
            status: "error",
            message: error.toString()
        })
    }
})

/**
router.get('/test', (req, res) => {
    res.status(200).send({
        code: 200,
        message: "test route success"
    })
})
 */

router.get('/:username', async(req, res) => {
    let code, status, message;

    // let UsersGetByusername = {}

    let username = req.params.username;
    await UsersFunc.UserGetByUsernameFunc(username, (data) => {
        if(!data.status) {
            code = 400;
            message = data.desc;
            status = "error";
        } else {
            code = 200;
            // items = data.data;
            status = "success";
            message = data.desc;
        }
        res.status(code).send({
            code:code,
            status:status,
            message:message,
            data:data.data
        })
    })
})

module.exports = router