var express = require('express');
const app = express();

app.use((req, res, next) => {
    if(!req.route) {
        res.status(404).send({
            code: 404,
            status: "ERROR",
            message: "No path found!"
        })
        // console.log(req)
    }
})

module.exports = app