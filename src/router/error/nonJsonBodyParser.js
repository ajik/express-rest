const express = require('express');
const app = express();

app.use((error, req, res, next) => {
    if(error.name === 'SyntaxError') {
        res.status(400).send({
            code: 400,
            message: 'Error 400: Bad Request',
            status: ''
        });
    }
    next();
    // console.info(req.methods);
})

module.exports = app