const express = require('express');
const app = express();

app.use((err, req, res, next) => {
    let authHeader = req.header("Authorization");
    console.log(`${authHeader} OIT OIT`)
    if(authHeader !== "") {
        next();
    } else {
        res.status(403).send({
            code: 403,
            message: "You have no authorization to access this API",
            status: "error"
        })
    }
})

app.use((err, req, res, next) => {

    if(err.name === 'UnauthorizedError') {
        res.status(401).send({
            code: 401,
            status: 'error',
            message: 'Invalid token!'
        })
    }
})

app.use((err, req, res, next) => {
    if(typeof (err) === 'string') {
        res.status(400).send({
            code: 400,
            status: 'error',
            message: 'Unknown Error!'
        })
    }
})

module.exports = app;