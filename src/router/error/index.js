const express = require('express');
const app = express();
const authError = require('./auth');
const nonBodyJsonBodyParserError = require('./nonJsonBodyParser')
const notFoundError = require('./NotFound')

app.use(authError)
app.use(nonBodyJsonBodyParserError)
app.use(notFoundError)

module.exports = app;