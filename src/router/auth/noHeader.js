const express = require('express')
const app = express()
const config = require('../../config/config.json')
const securityUtils = require('../../utils/security')


app.use(async (req, res, next) => {

    try {
        if(!req.get('Authorization')) {
            throw "You do not have permission to access this API!";
        } else {
            // extract bearer
            let authHeader = req.get('Authorization').substring(0, 6);
            if(authHeader === "Bearer") {
                let bearerAuthHeader = req.get('AUthorization');
                let token = bearerAuthHeader.substring(7, bearerAuthHeader.length);
                // securityUtils.validatingToken(token);

                await securityUtils.validatingToken(token, dataReturn => {
                    if(dataReturn.isValid) {
                        next();
                    } else {
                        res.status(403).send({
                            code: 403,
                            status: 'NOT AUTHORIZED!',
                            message: dataReturn.desc
                        })
                    }
                });
                // next();
            } else if(authHeader.substring(0,5) === "Basic") {
                let basicAuthHeader = req.get('Authorization');
                let token = basicAuthHeader.substring(6, basicAuthHeader.length);
                
                if(token === config.secretKey) {
                    next();
                } else {
                    throw "Basic token is invalid!";
                }
                // console.log("OITTTT");
            } else {
                throw "Token is invalid!";
            }
        }
    } catch(error) {
        res.status(403).send({
            code: 403,
            status: "error",
            message: error.toString()
        }).end();
    }
})

module.exports = app