const express = require('express')
const noHeader = require('./noHeader')
const app = express()

app.use(noHeader)

module.exports = app