var express = require('express')
var router = express.Router()

router.get('/', (req, res) => {
    res.status(200).send({
        status: "success get todos"
    })
})

router.get('/about', (req, res) => {
    res.status(200).send({
        status: "success get about todos"
    })
})

module.exports = router