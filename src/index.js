const todosRouter = require('./router/todos/todos')
const usersRouter = require('./router/users/users')
const express = require('express');
const compression = require('compression');
const winston = require('winston'), 
        expressWinston = require('express-winston');

const app = express();
const port = 3200;
const apiAddr = '/v1/api';
const winstonConfig = require('./utils/logger')
const securityUtils = require('./utils/security')
const nonJsonBodyParser = require('./router/error/nonJsonBodyParser')
const errorHandling = require('./router/error')
const auth = require('./router/auth')

app.use(compression())
app.use(express.urlencoded({
    extended: false
}))
app.use(express.json({
    type: () => {
        return true
    }
}))

// app.use(nonJsonBodyParser)

// for header security and prevent injection
// app.use(securityUtils.helmetSecurity())
// app.use(corsConfig)
app.use((req, res, next) => {
    winstonConfig.commonLogger(req, res, next)
})

app.use(expressWinston.logger(winstonConfig.loggerWinstonConfig()))

app.use(auth)
app.use(`${apiAddr}/todos`, todosRouter)
app.use(`${apiAddr}/users`, usersRouter)

// handling not found error
// app.use(NotFoundError)
app.use(errorHandling)


// error Logging
app.use(expressWinston.errorLogger(winstonConfig.expressWinstonErrorLogger()))

app.listen(port, () => {
    console.log('success to connect')
})