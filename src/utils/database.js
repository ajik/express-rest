const Pool = require('pg').Pool
const config = require('../config/config.json')


const pool = new Pool({
    user: config.user,
    host: config.host,
    database: config.dbName,
    password: config.password,
    port: config.port
})

pool.on('error', (error) => {
    console.error('cannot connect to DB')
})

module.exports = pool