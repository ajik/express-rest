const winston = require('winston')
const express = require('express')
const app = express()

module.exports = {
    loggerWinstonConfig: () => {
        let config = {
            transports: [
                // new winston.transports.Console(),
                new winston.transports.File({
                    dirname: 'logging',
                    filename: 'logging.log'
                }),
                new winston.transports.File({
                    dirname: 'logging',
                    filename: 'error.log',
                    level: 'error'
                }),
                new winston.transports.File({
                    dirname: 'logging',
                    filename: 'warn.log',
                    level: 'warn'
                })
            ],
            format: winston.format.combine(
                winston.format.colorize(),
                winston.format.simple()
            ),
            meta: true, // optional: control whether you want to log the meta data about the request (default to true)
            msg: "HTTP \n {{req.method}} \n {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
            expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
            colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
            ignoreRoute: (req, res) => { return false; },
            level: (req, res) => {
                let level = "";
        
                if(res.statusCode >= 100) {level = 'info'}
                if(res.statusCode >= 400) {level = 'warn'}
                if(res.statusCode >= 500) {level = 'error'}
        
                // this one below for old path
                // if(req.path === '/v1' && level === 'info) {level = 'warn'}
                return level;
            },
        };
        return config;
    },
    expressWinstonErrorLogger: () => {
        let config = {
            transports: [
                new winston.transports.Console()
            ],
            format: winston.format.combine(
                winston.format.colorize(),
                winston.format.json()
            )
        };
        return config;
    },
    commonLogger:(req, res, next) => {
        if(req.body) console.info(req.body);
        if(req.params) console.info(req.params);
        if(req.query) console.info(req.query);
        // console.info(req);
        console.info(`Methods: ${req.method} \n IP address: ${req.ip} \n Endpoint: ${req.url}`);
        next();
    }
}