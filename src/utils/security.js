const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const helmet = require('helmet')
const config = require('../config/config.json')
const pool = require('./database')
const queryBuilder = require('./buildQuery')

module.exports = {
    hashPassword: (password) => {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
    },
    comparePassword: (pass, hashPass) => {
        return bcrypt.compareSync(pass, hashPass);
    },
    isValidEmail: (email) => {
        return /\S+@\S+\.\S+/.test(email);
    },
    validatingToken: async (token, callback) => {
        let returnValue = {
            isValid: false,
            desc: ''
        };
        try {
            let decoded = jwt.verify(token, config.secretKey, {
                algorithm: [config.algorithm]
            });

            let query = queryBuilder.getUsersByUsernameQ;
            let username = decoded.username;
            // console.log(decoded);
            await pool.query(query, [username], (error, results) => {
                // console.log(results);
                if(results.rowCount < 1) {
                    throw "Token is Not Valid!";
                } else  if(results.rowCount === 1) {
                    returnValue.isValid = true;
                } else if(results.rowCount > 1) {
                    throw "Error Fetch Data";
                } else {
                    throw error.toString();
                }
                return callback(returnValue);
            });
        } catch(error) {
            // returnValue.isValid = false;
            returnValue.desc = error.toString();
            return callback(returnValue);
        }
        console.log(returnValue);
    },
    generateToken:(userId) => {

        let returnValue = {
            status: null,
            token: '',
            desc: ''
        };
        // console.log("Masuk sini")

        try {

            let token = jwt.sign({
                exp: Date.now() + config.expTime,
                username: userId
            }, config.secretKey, {
                algorithm: config.algorithm
            });

            returnValue.status = true;
            // console.log(config.value);
            returnValue.token = token;
        } catch(error) {
            returnValue.status = false;
            // console.log("catch");
            returnValue.desc = error;
        }
        return returnValue;
    },
    helmetSecurity:() => {
        return helmet()
    }  
}