// variable for table name
const usersTable = 'users'

// query variable and must exported in module.exports!!!
const getUsersByUsernameQ = `SELECT * FROM ${usersTable} WHERE user_id = $1`;
const PostUsersQ = `INSERT INTO users(id, email, email_verification_status, encrypted_password, first_name, last_name, user_id) VALUES ( nextVal('hibernate_sequence') ,$1, $2, $3, $4, $5, $6)`;
const getUsersQ = `SELECT * FROM ${usersTable}`
const getUsersByEmailAddressQ = `SELECT * FROM ${usersTable} WHERE email=$1`

module.exports = {
    getUsersByUsernameQ,
    PostUsersQ,
    getUsersQ,
    getUsersByEmailAddressQ
}